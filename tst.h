#ifndef JNP_TERNARY_SEARCH_TREE_TST_H
#define JNP_TERNARY_SEARCH_TREE_TST_H

#include <string>
#include <memory>
#include <iterator>

namespace Detail {
    // Funkcja fold na zakresie wyznaczonym przez iteratory działa następująco:
    //   functor(...functor(functor(acc, *first), *(first + 1))..., *(last - 1))
    // W szczególnym przypadku first == last fold zwraca acc.
    template<typename Iter, typename Acc, typename Functor>
    Acc fold(Iter first, Iter last, Acc acc, Functor functor) {
        return first == last ? acc :
               fold(std::next(first), last, functor(acc, *first), functor);
    }
}

template<typename C = char>
class TST {
    struct Node {
        const std::shared_ptr<const Node> lo_child;
        const std::shared_ptr<const Node> equal_child;
        const std::shared_ptr<const Node> hi_child;
        const C value;
        const bool end;

        Node(const std::shared_ptr<const Node>& lo_child,
             const std::shared_ptr<const Node>& equal_child,
             const std::shared_ptr<const Node>& hi_child,
             C value, bool end) :
                lo_child(lo_child), equal_child(equal_child),
                hi_child(hi_child), value(value), end(end) {}
    };

    const std::shared_ptr<const Node> root;

    void ensureNonEmpty() const {
        if (empty()) {
            throw EmptyTreeException();
        }
    }

    explicit TST(const std::shared_ptr<const Node>& root) : root(root) {}

    TST(const TST& lo, const TST& equal, const TST& hi, C value, bool end) : root(
            std::make_shared<Node>(lo.root, equal.root, hi.root, value, end)) {}

public:
    // Wyjątek zwracany w przypadku gdy próbujemy dostać się do pola pustego drzewa.
    class EmptyTreeException : public std::logic_error {
    public:
        EmptyTreeException() : std::logic_error("Tree is empty") {}
    };

    // Tworzy puste drzewo.
    TST() = default;

    // Tworzy zawierające tylko dany napis.
    TST(const std::basic_string<C>& str) : TST(str.c_str()) {}

    // Tworzy zawierające tylko dany napis.
    TST(const C* str) : TST(str[0] == '\0' ? TST() :
                            TST(TST(), TST(str + 1), TST(), str[0], str[1] == '\0')) {}

    // Zwraca drzewo z dodanym słowem.
    TST operator+(const std::basic_string<C>& str) const {
        return operator+(str.c_str());
    }

    // Zwraca drzewo z dodanym słowem.
    TST operator+(const C* str) const {
        return str[0] == '\0' ? *this :
               empty() ? TST(TST(), TST(str + 1), TST(), str[0], str[1] == '\0') :
               str[0] == value() ? TST(left(), center() + (str + 1),
                                       right(), value(), word() || str[1] == '\0') :
               str[0] < value() ? TST(left() + str, center(), right(), value(), word()) :
               TST(left(), center(), right() + str, value(), word());

    }

    // Zwraca literę przechowywaną w danym węźle.
    C value() const {
        ensureNonEmpty();
        return root->value;
    }

    // Sprawdza, czy w danym węźle kończy się słowo.
    bool word() const {
        ensureNonEmpty();
        return root->end;
    }

    // Zwraca lewe poddrzewo.
    TST left() const {
        ensureNonEmpty();
        return TST(root->lo_child);
    }

    // Zwraca środkowe poddrzewo.
    TST center() const {
        ensureNonEmpty();
        return TST(root->equal_child);
    }

    // Zwraca prawe poddrzewo.
    TST right() const {
        ensureNonEmpty();
        return TST(root->hi_child);
    }

    // Sprawdza, czy drzewo jest puste.
    bool empty() const {
        return !root;
    }

    // Sprawdza, czy drzewo zawiera dane słowo.
    bool exist(const std::basic_string<C>& str) const {
        return exist(str.c_str());
    }

    // Sprawdza, czy drzewo zawiera dane słowo.
    bool exist(const C* str) const {
        return str[0] == '\0' ? false :
               empty() ? false :
               str[0] == value() ? (str[1] == '\0' ? word() : center().exist(str + 1)) :
               str[0] < value() ? left().exist(str) :
               right().exist(str);
    }

    // Wyszukuje najdłuższy wspólny prefiks słowa str i słów zawartych w danym
    // drzewie. Przykład: jeśli tst składa się ze słów "category", "functor"
    // oraz "theory" to tst.prefix("catamorphism") daje rezultat "cat".
    std::basic_string<C> prefix(const std::basic_string<C>& str) const {
        return prefix(str.c_str());
    }

    // Wyszukuje najdłuższy wspólny prefiks słowa str i słów zawartych w danym
    // drzewie. Przykład: jeśli tst składa się ze słów "category", "functor"
    // oraz "theory" to tst.prefix("catamorphism") daje rezultat "cat".
    std::basic_string<C> prefix(const C* str) const {
        return str[0] == '\0' ? std::basic_string<C>() :
               empty() ? std::basic_string<C>() :
               str[0] == value() ? std::basic_string<C>(1, value())
                       .append(center().prefix(str + 1)) :
               str[0] < value() ? left().prefix(str) :
               right().prefix(str);
    }

    // Wykonuje daną funkcję rekurencyjnie dla poddrzew.
    // Dla liści zwraca akumulator.
    template<typename Acc, typename Functor>
    Acc fold(Acc acc, Functor functor) const {
        return empty() ? acc :
               functor(*this, left().fold(acc, functor),
                       center().fold(acc, functor), right().fold(acc, functor));
    }

    // Zwraca rozmiar drzewa w literach.
    size_t size() const {
        return this->fold(0u, [](TST<C>, size_t lo, size_t equal, size_t hi) {
            return 1 + lo + equal + hi;
        });
    }
};

#endif //JNP_TERNARY_SEARCH_TREE_TST_H
