#include "tst.h"
#include <iostream>
#include <cassert>
#include <vector>

using namespace std;

int main() {
    int v[3]= {3, 1, 2};
    cout << Detail::fold(v, v+3, 1, [](auto acc, auto x){return acc * x;});
    vector<int> x = {3, 1, 2};
    cout << Detail::fold(x.end(), x.end(), 1, [](auto acc, auto x){return acc * x;});
    return 0;
}